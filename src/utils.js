export const formatPercentage = (float) => `${(parseFloat(float, 2) * 100).toFixed(2)}%`;
export const formatNumber = (number) => number.toLocaleString();
export const formatGBP = (value) => `${parseFloat(value).toLocaleString("en-GB", {style: "currency", currency: "GBP", minimumFractionDigits: 2})}`;
