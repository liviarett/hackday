import React, { Fragment, Component } from "react";
import Header from "./Header/Header";
import Content from "./ContentItem/Content";

// Rename this component to TableManager os something like that

class Table extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      sort: {
        sortByPlatform: '',
        sortByData: 'name',
        sortAscending: true,
      }, 
      togglingCategories: false,
      filtering: false,
      hasError: false,
    }
  }

  componentDidMount() {
    fetch("http://my-json-server.typicode.com/gusreed/json-server-test/summary")
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw "Error!";
        }
      })
      .then(data =>
      {
        this.setState({ data })
      })
      .then(() => {
        const dataState = this.state.data.map(item => ({
          ...item,
          isVisible: true,
          isCollapsed: true,
          totalGp: item.platforms.reduce((acc, platform) => {
            return acc + parseInt(platform.gp_uplift);
          }, 0)
        }));
        this.setState({
          ...this.state,
          data: dataState,
          hasError: false,
        });
      })
      .catch(err => {
        this.setState({
          data: [],
          hasError: true,
        });
      });
  }

  toggleCategoriesMenu() {
    this.setState({
      ...this.state,
      togglingCategories: !this.state.togglingCategories
    });
  }

  sortBy(data, platform) {
    const ascending = (
      (this.state.sort.sortByData == data &&
        this.state.sort.sortByPlatform == platform) ?
        !this.state.sort.sortAscending : true
      );
    const mapped = this.state.data.map(function (item, i) {
      if (!platform) {
        return { index: i, id: item.id, value: item[data]};
      } else if (platform && data) {
        const value = item.platforms.filter(platformObj => platformObj.id === platform)[0][data];
        return { index: i, id: item.id, value }
      }
    })
    
    mapped.sort(function (a, b) {
      if (!parseInt(a.value) && parseInt(a.value) !== 0) {
        [a, b] = [{...b, value: b.value.toLowerCase()}, {...a, value: a.value.toLowerCase()}];
      }

      if (a.value > b.value) {
        return 1;
      }
      if (a.value < b.value) {
        return -1;
      }
      return 0;
    });
    
    if (ascending) {
      mapped.reverse();
    }
    
    const newDataState = mapped.map(mappedItem =>
      this.state.data.filter(item => item.id === mappedItem.id)[0]
    )

    this.setState({
      ...this.state,
      data: newDataState,
      sort: {
        sortByPlatform: platform,
        sortByData: data,
        sortAscending: ascending
      }
    });
  }
  
  filterList(input) {
    const newDataState = this.state.data.map(item => {
        const isVisible = item.name.toLowerCase().indexOf(input.toLowerCase()) >= 0 ? true : false;
        return {
          ...item,
          isVisible
        };
      });
      const filtering = input != '';
      this.setState({
      ...this.state,
      data: newDataState,
      filtering
    });
  }

  collapseInfo(category) {
    const newDataState = this.state.data.map(
      item =>
        item.name === category ? { ...item, isCollapsed: !item.isCollapsed } : item
    );
    this.setState({
      ...this.state,
      data: newDataState
    });
  }

  collapseAll(isCollapsed) {
    const newDataState = this.state.data.map(item => ({ ...item, isCollapsed }));
    this.setState({
      ...this.state,
      data: newDataState
    });
  }

  hideCategories(category) {
    const newDataState = this.state.data.map(
      item =>
        item.name === category ? { ...item, isVisible: !item.isVisible } : item
    );
    this.setState({
      ...this.state,
      data: newDataState
    });
  }

  toggleAllCategories(isVisible) {
    const newDataState = this.state.data.map(item => ({ ...item, isVisible }));
    this.setState({
      ...this.state,
      data: newDataState,
      filtering: false,
    });
  }

  render() {
    const categories = this.state.data.map(item => ({
      id: item.id,
      name: item.name,
      isVisible: item.isVisible
    }));
    const listOfPlatforms = Array.from(new Set(...this.state.data.map(item => item.platforms.map(platform => platform.id))));
    const areAllCollapsed = this.state.data.every(item => item.isCollapsed)
    const areAllOpen = this.state.data.every(item => !item.isCollapsed)

    // FIXME: MAKE SURE TO ORDER ALL DATA IN THE RIGHT PLATFORM ORDER - set listOfPlatforms as a state, 
    // then render each platform according to the list order, 
    // ie listOfPlatforms.map(platformListItem => platforms.map(
    // platform => platformListItem === platform.id && <PlatformInfo />)
    // )

    return (
      <Fragment>
        <Header
          categories={categories}
          collapsed={!this.state.togglingCategories}
          areAllCollapsed={areAllCollapsed}
          areAllOpen={areAllOpen}
          hideCategories={this.hideCategories.bind(this)}
          toggle={this.toggleCategoriesMenu.bind(this)}
          toggleAll={this.toggleAllCategories.bind(this)}
          collapseAll={this.collapseAll.bind(this)}
          filterList={this.filterList.bind(this)}
          filtering={this.state.filtering}
        />
        {!this.state.hasError ? (
          <Content
            listOfPlatforms={listOfPlatforms}
            dataToShow={this.state.data}
            collapse={this.collapseInfo.bind(this)}
            sortBy={this.sortBy.bind(this)} 
            sortState={this.state.sort}
          />
        ) : (
          <div>Unable to get data at this moment</div>
          )
        }
      </Fragment>
    );
  }
}

export default Table;
