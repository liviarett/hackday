import React from "react";

const Filter = ({ filter, filtering }) => (
  <div className={`filter-wrapper ${filtering ? 'active' : ''}`}>
  <span className="filter-title">Filter by category</span>
    <input className="filter-input" type="text" onKeyUp={e => filter(e.target.value)} />
  </div>
);

export default Filter;
