import React, {Fragment} from "react";
import Filter from "./Filter";

const Header = ({
  categories,
  collapsed,
  areAllCollapsed,
  areAllOpen,
  hideCategories,
  toggle,
  toggleAll,
  collapseAll,
  filterList,
  filtering,
}) => (
  <header>
  <div className="header-top"><img className="logo-fiddler" src="./logo-fiddler.png"/></div>
    {!collapsed ? (
      <div className="toggle-menu-wrapper">
        <button className="toggle-button close-panel" onClick={() => toggle()}>×</button>
        <button className="toggle-button toggle-all hide" onClick={() => toggleAll(false)}>hide all</button>
          <button className="toggle-button toggle-all show" onClick={() => toggleAll(true)}>show all</button>
          <div className="toggle-categories-wrapper">
          {categories.map((category, index) => (
              <div category-id={category.id} className={`toggle-category ${category.isVisible ? 'toggle-visible' : 'toggle-hidden'}`} key={index} onClick={() => hideCategories(category.name)}>
              {category.name}
            </div>
          ))}
          </div>  
      </div>
      ) : (
          <Fragment>
          <button className="header-button toggle-menu-button" onClick={() => toggle()}>Toggle Categories</button>
          <button className={`header-button collapse-all collapse ${areAllOpen ? 'active' : ''}`} onClick={() => collapseAll(false)}>Open all</button>    
            <button className={`header-button collapse-all show ${areAllCollapsed ? 'active' : ''}`} onClick={() => collapseAll(true)}>Collapse all</button>    
           <Filter filter={filterList} filtering={filtering} />
          </Fragment>    
    )}
  </header>
);

export default Header;

// TODO: MOVE BUTTONS TO DIFFERENT COMPONENT