import React from "react";

const ErrorMessage = () => (
  <div className="error-message">Unable to load data at this time.</div>
)

export default ErrorMessage;