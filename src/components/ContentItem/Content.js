import React from "react";
import Overview from './Overview';
import CategoryInfo from './CategoryInfo';
import ContentHeader from './ContentHeader';

const Content = ({ listOfPlatforms, dataToShow, collapse, sortBy, sortState }) => {
  return dataToShow.some(item => item.isVisible) ? (
    <div className="content-wrapper">
      <ContentHeader listOfPlatforms={listOfPlatforms} sortBy={sortBy} sortState={sortState}/>  
      {dataToShow.map(
        (item, index) => 
          item.isVisible && (
            <div key={index} className="content-item">
            <Overview item={item} collapse={collapse} /> 
             {(!item.isCollapsed && <CategoryInfo item={item}/>)}
            </div>
          )
      )}
    </div>
  ) : (
    <div className="no-content">Please select something to show</div>
  );
}
export default Content;
