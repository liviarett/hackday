import React, { Component, Fragment } from "react";
import PlatformInfo from './PlatformInfo';
import LoadingMessage from './LoadingMessage';
import ErrorMessage from './ErrorMessage';

// TODO: Call this component CategoryInfoManager
class CategoryInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      adData: {},
      isLoading: true,
      hasError: false,
    }
  }

  componentWillMount() {
    // TODO: this info could be added to TABLE.JS state, so that it only gets fetched from the server once
    // on ComponentDidMount then sets the Table's state.
    fetch(`http://my-json-server.typicode.com/gusreed/json-server-test/details/${this.props.item.id}`)
      .then(response => {
        return response.json()
      })
    .then(adData =>
    {
      this.setState({
        ...this.state,
        adData,
        isLoading: false,
        hasError: false,
      })
    })
    .catch(err => { 
      this.setState({
        ...this.state,
        isLoading: false,
        hasError: true,
      })
    })
  }

  render() {
    const { name, platforms } = this.state.adData;
    const fieldsToRender = ['PERIOD', 'AD', 'CTR', 'CONVERSIONS', 'CPA', 'CLICKS', 'SPEND', 'DATE UPLOADED'];
    if (this.state.isLoading) {
      return <LoadingMessage />;
    }

    if (this.state.hasError) {
      return <ErrorMessage />;
    }

    return (
      // TODO: Make this a new component - CategoryWrapper.js, 
      // then move it to a folder "CategoryData" together with LoadingMessage and ErrorMessage
      <table
          className="item-ad-info"
      >
        {
          // Move this to CategoryHeader.js
        }
          <thead>
            <tr className="item-info-header">
            {
              fieldsToRender.map(field => <th key={field}>{field}</th>)
            }
            </tr>
          </thead>
        {
          !!platforms &&
          platforms.map(platform => (
            <tbody key={platform.id}>
              {
                // Move whole thing to PlatformWrapper.js
              }
              {
                // Move this to PlatformHeader.js
              }  
                <tr>
                  <td colSpan="100%" className="platform-name">{platform.name.toUpperCase()}</td>
                </tr>
                <PlatformInfo segments={platform.segments} />
              </tbody>
            ))
        }
      </table>
    )
  }
}

export default CategoryInfo;