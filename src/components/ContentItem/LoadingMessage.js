import React from "react";

const LoadingMessage = () => (
  <div className="loading-message">Loading...</div>
)

export default LoadingMessage;
