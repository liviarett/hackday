import React from "react";

const ContentHeader = ({ listOfPlatforms, sortBy, sortState }) => {

  const isSortingClass = (field, platform) =>
  sortState.sortByPlatform == platform &&
      sortState.sortByData === field ? `active ${sortState.sortAscending ? 'ascending' : ''}` : '';
  const isSortingArrow = (field, platform) =>
    sortState.sortByPlatform == platform &&
      sortState.sortByData === field ? sortState.sortAscending ? '▼' : '▲' : '▼';
  // Move button to SortingButton.js
  return (
  <div className="table-header list-item">
      <div className="top-line-header-wrapper column-title">Campaign Name <button className={`sort-button ${isSortingClass('name', null)}`} onClick={() => sortBy('name', null)}>{isSortingArrow('name', null)}</button></div>    
      {listOfPlatforms.map(platform => {
        return (
      <div key={platform} className="top-line-header-wrapper column-content">
      <div className={`top-line-header top-line-header-platform background-platform-${platform}`}>{platform.toUpperCase()}</div>
        <div className="top-line-header top-line-info">GP <button className={`sort-button ${isSortingClass('gp_uplift', platform)}`} onClick={() => sortBy('gp_uplift', platform)}>{isSortingArrow('gp_uplift', platform)}</button></div>
      <div className="top-line-header top-line-info">CPA <button className={`sort-button ${isSortingClass('cpa_change', platform)}`} onClick={() => sortBy('cpa_change', platform)}>{isSortingArrow('cpa_change', platform)}</button></div>
    </div>
    )})}
      <div className="top-line-header-wrapper column-title">Total GP Uplift <button className={`sort-button ${isSortingClass('totalGp', null)}`} onClick={() => sortBy('totalGp', null)}>{isSortingArrow('totalGp', null)}</button></div>  
</div>
)
};

export default ContentHeader;
