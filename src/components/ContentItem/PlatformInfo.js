import React, { Fragment } from "react";
import { formatPercentage, formatNumber, formatGBP } from './../../utils';

const PlatformInfo = ({ segments }) => (
  segments.map(segment => {
    return (
      <tr className="segment-wrapper" key={segment.id}>
      <td className="timeline">{segment.name.toUpperCase()}</td>
        <td><img className="ad-image" src={segment.image} />
          <a href={segment.link}><span className="ad-link">{segment.copy}</span></a>
        </td>  
      <td>{formatPercentage(segment.ctr)}</td>
      <td>{formatNumber(segment.conversions)}</td>
      <td>{formatGBP(segment.cpa)}</td>
      <td>{formatNumber(segment.clicks)}</td>
      <td>{formatGBP(segment.spend)}</td>
      <td>{new Date(segment.uploaded_at).toLocaleDateString()}</td>
      </tr>
  )})
);

export default PlatformInfo;