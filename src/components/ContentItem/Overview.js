import React from "react";
import { formatNumber, formatGBP, formatPercentage } from '../../utils';

const Overview = ({ item, collapse }) => {
  return (
  <div
    category-id={item.id}
    className="item-overview list-item"  
    onClick={() => collapse(item.name)}
    >
    <h3 className="overview-title column-title">{item.name}</h3>
    {item.platforms.map(platform => (
      <div key={platform.id} className="column-content">
          <div className="top-line-info">{formatGBP(platform.gp_uplift)}</div>
          <div className="top-line-info">{(formatPercentage(platform.cpa_change))}</div>
      </div>
      ))}    
      <div className="column-content">{formatGBP(item.totalGp)}</div>  
    </div>
  )
}
;

export default Overview;
